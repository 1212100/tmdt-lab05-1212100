﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI1212100.Models;

namespace WebAPI1212100.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductController : ApiController
    {
        [HttpGet]
        [Route("api/2.0/products/{category}")]
        public IHttpActionResult GetProductByCategory1212100(int category)
        {
            using (Lab5Entities db = new Lab5Entities())
            {
                var list = db.Products.Where(p => p.categoryid == category).ToList();
                if (list != null)
                {
                    List<ProductModel> listofProduct = new List<ProductModel>();
                    ProductModel productmodel = new ProductModel();
                    foreach (var product in list)
                    {
                        productmodel.id = product.id;
                        productmodel.name = product.name;
                        productmodel.price = product.price;
                        productmodel.categoryid = product.categoryid;
                        productmodel.userid = product.userid;

                        listofProduct.Add(productmodel);
                    }

                    IEnumerable<ProductModel> products = listofProduct;

                    return Ok(products);
                }

                return NotFound();

            }
        }




        [HttpGet]
        [Route("api/2.0/products/{category}/{username}")]
        public IHttpActionResult GetProductByCategoryAndUser1212100(int category, string username)
        {

            using (Lab5Entities db = new Lab5Entities())
            {
                var list = db.Products.Where(p => p.categoryid == category && p.userid.Equals(username)).ToList();
                if (list != null)
                {
                    List<ProductModel> listofProduct = new List<ProductModel>();
                    ProductModel productmodel = new ProductModel();
                    foreach (var product in list)
                    {
                        productmodel.id = product.id;
                        productmodel.name = product.name;
                        productmodel.price = product.price;
                        productmodel.categoryid = product.categoryid;
                        productmodel.userid = product.userid;

                        listofProduct.Add(productmodel);
                    }

                    IEnumerable<ProductModel> products = listofProduct;

                    return Ok(products);
                }

                return NotFound();

            }
        }
    }
}
