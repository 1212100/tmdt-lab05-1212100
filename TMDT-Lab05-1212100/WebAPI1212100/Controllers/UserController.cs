﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI1212100.Models;

namespace WebAPI1212100.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        [HttpGet]
        [Route("api/2.0/users")]
        public IHttpActionResult GetAll1212100()
        {
            using (Lab5Entities db = new Lab5Entities())
            {
               

                List<User> list = db.Users.ToList();
                if (list != null)
                {
                    AutoMapper.Mapper.CreateMap<User, UserModel>();
                    List<UserModel> ret = AutoMapper.Mapper.Map<List<User>, List<UserModel>>(list);

                    return Ok(ret);
                }

                return NotFound();

            }

        }



        [HttpGet]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult GetById1212100(string username)
        {
            using (Lab5Entities db = new Lab5Entities())
            {
                User userbyid = db.Users.Where(p => p.username.Equals(username)).SingleOrDefault();

                if (userbyid != null)
                {
                    UserModel usermodel = new UserModel();
                    usermodel.username = userbyid.username;
                    usermodel.password = userbyid.password;


                    return Ok(usermodel);
                }

                return NotFound();


            }
        }









        [HttpPost]
        [Route("api/2.0/users")]
        public IHttpActionResult AddUser1212100([FromBody]User newuser)
        {

            using (Lab5Entities db = new Lab5Entities())
            {
                db.Users.Add(newuser);
                int indexchange = db.SaveChanges();

                return Ok(indexchange);
            }

        }


        [HttpDelete]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult DeleteUser1212100(string username)
        {
            using (Lab5Entities db = new Lab5Entities())
            {
                var userbyusername = db.Users.Where(u => u.username.Equals(username)).SingleOrDefault();
                if (userbyusername != null)
                {
                    db.Users.Remove(userbyusername);
                    int indexchange = db.SaveChanges();
                    return Ok(indexchange);
                }


                return NotFound();
            }
        }



        [HttpPut]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult UpdateUser1212100(string username, [FromBody]string password)
        {
            using (Lab5Entities db = new Lab5Entities())
            {
                var userbyusername = db.Users.Where(u => u.username.Equals(username)).SingleOrDefault();
                if (userbyusername != null && password != null)
                {
                    userbyusername.password = password;
                    int indexchange = db.SaveChanges();
                    return Ok(indexchange);
                }


                return NotFound();
            }
        }
    }
}
