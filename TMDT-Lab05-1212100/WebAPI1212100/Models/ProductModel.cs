﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212100.Models
{
    public class ProductModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public string userid { get; set; }
        public int categoryid { get; set; }
    }
}