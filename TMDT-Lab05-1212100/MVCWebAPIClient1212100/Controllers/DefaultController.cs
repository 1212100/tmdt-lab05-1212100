﻿using MVCWebAPIClient1212100.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212100.Controllers
{
    public class DefaultController : Controller
    {
        //
        // GET: /Default/
        public ActionResult Index()
        {
            GetAllUser list = new GetAllUser();
            list.listUsers = new List<NguoiDung>();
            return View(list);
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.GetAsync("api/2.0/users").Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    GetAllUser list = new GetAllUser();
                    list.listUsers = new List<NguoiDung>();
                    list.listUsers = response.Content.ReadAsAsync<List<NguoiDung>>().Result;
                    return View(list);
                }
            }
            return View();
        }

        public ActionResult GetUserByUsername()
        {
            NguoiDung nd = new NguoiDung();
            return View(nd);
        }

        [HttpPost]
        public ActionResult GetUserByUsername(FormCollection form)
        {
            string strUsername = form["txtUsername"].ToString();
            NguoiDung nd = new NguoiDung();

            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.GetAsync("api/2.0/users/" + strUsername).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    
                    nd = response.Content.ReadAsAsync<NguoiDung>().Result;
                    return View(nd);
                }
            }
            return View(nd);
        }



        public ActionResult UpdateUser()
        {
            ViewBag.MS = "";
            return View();
        }

        [HttpPost]
        public ActionResult UpdateUser(FormCollection form)
        {
            string strUsername = form["txtUsernamePut"].ToString();
            string strNewPassword = form["txtNewPasswordPut"].ToString();

            ViewBag.MS = "";
            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.PutAsJsonAsync("api/2.0/users/" + strUsername,strNewPassword).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ViewBag.MS = "Success Update";
                    
                    return View();
                }
            }
            return View();
        }



        public ActionResult DeleteUser()
        {
            ViewBag.MS = "";
            return View();
        }

        [HttpPost]
        public ActionResult DeleteUser(FormCollection form)
        {
            string strUsername = form["txtUsernameDelete"].ToString();
            

            ViewBag.MS = "";
            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.DeleteAsync("api/2.0/users/" + strUsername).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ViewBag.MS = "Success Delete";

                    return View();
                }
            }
            return View();
        }



        public ActionResult AddUser()
        {
            ViewBag.MS = "";
            return View();
        }

        [HttpPost]
        public ActionResult AddUser(FormCollection form)
        {
            string strUsername = form["txtNewUsername"].ToString();
            string strPassword = form["txtNewPassword"].ToString();


            ViewBag.MS = "";
            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

                var model = new {username = strUsername, password = strPassword };

                HttpResponseMessage response = c.PostAsJsonAsync("api/2.0/users", model).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    ViewBag.MS = "Success Add";

                    return View();
                }
            }
            return View();
        }



	}
}