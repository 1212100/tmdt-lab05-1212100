﻿using MVCWebAPIClient1212100.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212100.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.GetAsync("api/2.0/users").Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    List<NguoiDung> list = response.Content.ReadAsAsync<List<NguoiDung>>().Result;

                    ViewBag.MS = list[0].username;
                    return View();
                }
            }
            return View();
        }

      
    }
}