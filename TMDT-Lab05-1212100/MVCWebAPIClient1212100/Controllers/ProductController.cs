﻿using MVCWebAPIClient1212100.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212100.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult GetProductByCategory()
        {
            GetAllProduct list = new GetAllProduct();
            list.listofProduct = new List<SanPham>();
            return View(list);
        }

        [HttpPost]
        public ActionResult GetProductByCategory(FormCollection form)
        {
            GetAllProduct list = new GetAllProduct();
            list.listofProduct = new List<SanPham>();

            string strCategory = form["txtCategory"].ToString();

            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = c.GetAsync("api/2.0/products/" + strCategory).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                   
                    list.listofProduct = response.Content.ReadAsAsync<List<SanPham>>().Result;
                    return View(list);
                }
            }
            return View(list);
        }









        public ActionResult GetProductByCategoryAndUsername()
        {
            GetAllProduct list = new GetAllProduct();
            list.listofProduct = new List<SanPham>();
            return View(list);
        }

        [HttpPost]
        public ActionResult GetProductByCategoryAndUsername(FormCollection form)
        {
            GetAllProduct list = new GetAllProduct();
            list.listofProduct = new List<SanPham>();

            string strCategorySearch = form["txtCategorySearch"].ToString();
            string strUsernameSearch = form["txtUsernameSearch"].ToString();
            

            using (var c = new HttpClient())
            {
                c.BaseAddress = new Uri("http://localhost:22337/");
                c.DefaultRequestHeaders.Accept.Clear();
                c.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

               
                HttpResponseMessage response = c.GetAsync("api/2.0/products/" + strCategorySearch + "/" + strUsernameSearch).Result;
                if (response.StatusCode == HttpStatusCode.OK)
                {

                    list.listofProduct = response.Content.ReadAsAsync<List<SanPham>>().Result;
                    return View(list);
                }
            }
            return View(list);
        }
	}
}